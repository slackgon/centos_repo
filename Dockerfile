FROM centos:7.9.2009

RUN yum install -y openssh-server 
RUN  mkdir /root/.ssh
RUN  chmod 700 /root/.ssh

RUN sed -i 's/#PermitRootLogin yes/PermitRootLogin prohibit-password/g' /etc/ssh/sshd_config 
RUN yum  install -y httpd 
RUN yum install -y createrepo
RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
   

EXPOSE 22 80

CMD ["/usr/sbin/sshd","-D"]

